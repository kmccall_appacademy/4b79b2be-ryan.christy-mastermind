
PEGS = {red: "r", green: "g", blue: "b", yellow: "y", orange: "o", purple: "p"}
class Code
  attr_reader :pegs

  PEGS = {red: "r", green: "g", blue: "b", yellow: "y", orange: "o", purple: "p"}

  def initialize(arr)
    raise "error" if arr.nil?
    @pegs = arr
  end

  #builds a code instance with random PEG colors
  def self.random
    return Code.new(["r","g","b","y","o","p"].shuffle[0..3])
  end

#takes a user input string and build a Code object
  def self.parse(input)
    raise "error" if input.chars.any?{|el| !PEGS.values.include?(el.downcase)}
    return Code.new(input.chars.map(&:downcase))
  end

#both the secret code and the user's guess should be Code objects
#these two handle comparisons

  def [](arg)
    @pegs[arg]
  end

  def exact_matches(other_code)
    count = 0
    0.upto(3) { |i| count += 1 if @pegs[i] == other_code.pegs[i] }

    count
  end

  def near_matches(other_code)
    comp_arr = @pegs.dup
    count = 0
    other_code.pegs.each.with_index do |el, idx|
      count += 1 if comp_arr.include?(el)
      if !comp_arr.index(el).nil?
        idx1 = comp_arr.index(el)
        comp_arr.delete_at(idx1)
      end
    end

    count - exact_matches(other_code)
  end

  def == (other_code)
    return false if !other_code.is_a?(Code)
    @pegs == other_code.pegs
  end

end


#keeps track of how many turns has passed
#the correct Code
#and prompts the user for input

class Game
  attr_reader :secret_code

  def initialize(arg=Code.random)
    @secret_code = arg
  end

  def get_guess
    input = gets.chomp
    Code.new(input)
  end

  def display_matches(other_code)
    puts "exact matches: #{@secret_code.exact_matches(other_code)}, near matches: #{@secret_code.near_matches(other_code)}"
  end

end
